package nl.praegus.travel.test.stepdefinition;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.UnreachableBrowserException;

import java.io.File;
import java.util.Arrays;

public class CucumberSteps {
    private static WebDriver driver;

    public static synchronized WebDriver getWebdriver() {
        try {
            if (driver == null) {
                String os = System.getProperty("os.name");
                String chromedriver = "chromedriver-linux-64bit";
                ChromeOptions options = new ChromeOptions();
                if (os.contains("Windows")) {
                    chromedriver = "chromedriver-windows-73.exe";
                    options.setBinary(new File("C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"));
                }
                System.setProperty("webdriver.chrome.driver", "tools/" + chromedriver);
                options.addArguments(Arrays.asList("chrome.switches", "--disable-extensions"));
                driver = new ChromeDriver(options);
                return driver;
            } else {
                return driver;
            }
        } finally {
            Runtime.getRuntime().addShutdownHook(new Thread(new BrowserCleanup()));
        }
    }

    private static class BrowserCleanup implements Runnable {
        private static void close() {
            try {
                driver.quit();
                driver = null;
            } catch (UnreachableBrowserException e) {
                System.out.println("cannot close browser: unreachable browser");
            }
        }

        public void run() {
            close();
        }
    }
}
