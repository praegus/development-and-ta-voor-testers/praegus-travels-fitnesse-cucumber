package nl.praegus.travel.test.stepdefinition;

import cucumber.api.java.nl.Dan;
import cucumber.api.java.nl.En;
import cucumber.api.java.nl.Gegeven;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class Zoekpagina extends CucumberSteps {

    @Gegeven("^ik op de homepage ben$")
    public void ikOpDeHomepageBen() {
        getWebdriver().get("http://localhost:8080/index.html");
    }

    @En("^ik selecteer land \"([^\"]*)\"$")
    public void ikSelecteer(String land) {
        new Select(getWebdriver().findElement(By.xpath("//select[@prg_name='country']"))).selectByVisibleText(land);
    }

    @En("^ik selecteer zwembad \"([^\"]*)\"$")
    public void ikSelecteerZwembad(String zwembad) {
        new Select(getWebdriver().findElement(By.xpath(""))).selectByVisibleText(zwembad);
    }

    @Dan("^vind ik camping \"([^\"]*)\"$")
    public void vindIkCamping(String campingnaam) throws Throwable {
        Thread.sleep(3000);
        List<WebElement> elements = getWebdriver().findElements(By.linkText(campingnaam));
        assertThat(elements.size()).isEqualTo(1);
    }

    @En("^ik kies zoeken$")
    public void ikKiesZoeken() {
        By zoekCriterium = By.className("searchbutton");
        getWebdriver().findElement(zoekCriterium).click();
    }
}
