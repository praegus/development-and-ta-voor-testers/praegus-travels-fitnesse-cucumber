#language: nl
Functionaliteit: Testen Praegus travel app zoekfunctionaliteit

  Scenario: Zoeken op belgie
    Gegeven ik op de homepage ben
    En ik selecteer land "Belgie"
    En ik kies zoeken
    Dan vind ik camping "Foxworth"
