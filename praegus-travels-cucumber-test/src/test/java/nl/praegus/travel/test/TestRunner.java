package nl.praegus.travel.test;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/nl/praegus/travel/test/feature",
        glue = ""
)

public class TestRunner {

}
