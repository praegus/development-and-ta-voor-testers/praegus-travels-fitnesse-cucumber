package nl.praegus.travels.backend;

public class AuthorizationFailedException extends Throwable {
    public AuthorizationFailedException(String message) {
        super(message);
    }
}
