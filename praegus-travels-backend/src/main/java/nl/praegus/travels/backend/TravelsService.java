package nl.praegus.travels.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.praegus.travels.backend.entity.Camping;
import nl.praegus.travels.backend.entity.Campings;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
class TravelsService {
    private Campings campings;

    TravelsService() throws IOException {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream inJson = classloader.getResourceAsStream("campings.json");
        this.campings = new ObjectMapper().readValue(inJson, Campings.class);
    }

    Campings get(String country, String name, Boolean hasPool, Boolean hasAnimation) {
        Stream<Camping> campingsGefilterd = campings.getCampings().stream();

        if (country != null) {
            campingsGefilterd = campingsGefilterd.filter(c -> c.getCountry() != null && c.getCountry().equals(country));

        }
        if (name != null) {
            campingsGefilterd = campingsGefilterd.filter(c -> c.getName() != null && c.getName().equals(name));
        }
        if (hasPool != null) {
            campingsGefilterd = campingsGefilterd.filter(c -> c.isHasPool() == hasPool);
        }
        if (hasAnimation != null) {
            campingsGefilterd = campingsGefilterd.filter(c -> c.isHasAnimation() == hasAnimation);
        }

        return new Campings(campingsGefilterd.collect(Collectors.toList()));
    }

    void post(Camping camping) {
        camping.set_id(UUID.randomUUID().toString());
        camping.setIndex(campings.getCampings().size() + 1);
        campings.add(camping);

    }

    void delete(int index) {
        campings.remove(index);
    }
}
