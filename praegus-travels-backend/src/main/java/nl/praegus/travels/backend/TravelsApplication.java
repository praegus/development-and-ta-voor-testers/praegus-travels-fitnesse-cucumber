package nl.praegus.travels.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.praegus.travels.backend.entity.Camping;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ServerPropertiesAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import java.io.InputStream;

//@EnableAutoConfiguration
@Import({
    EmbeddedServletContainerAutoConfiguration.class,
    ServerPropertiesAutoConfiguration.class,
    DispatcherServletAutoConfiguration.class,
    TravelsConfiguration.class
})

public class TravelsApplication {


    public static void main(String[] args) {
        SpringApplication.run(TravelsController.class, args);
    }
}
