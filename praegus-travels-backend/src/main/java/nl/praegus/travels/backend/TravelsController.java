package nl.praegus.travels.backend;

import nl.praegus.travels.backend.entity.Camping;
import nl.praegus.travels.backend.entity.Campings;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;

@Controller
@EnableAutoConfiguration
public class TravelsController {
    //Lets use some hardcoded credentials
    private static final String httpUsername = "Tester";
    private static final String httpPassword = "Test123";

    private final TravelsService travelsService;

    public TravelsController() throws IOException {
        this.travelsService = new TravelsService();
    }

    @GetMapping(value = "/campings", produces = "application/json;charset=UTF-8")
    @ResponseBody
    @CrossOrigin
    public Campings getCampings(@RequestHeader(name = "Authorization", required = false) String authorizationHeader,
                                @RequestParam(value = "name", required = false) String name,
                                @RequestParam(value = "country", required = false) String country,
                                @RequestParam(value = "hasPool", required = false) String hasPool,
                                @RequestParam(value = "hasAnimation", required = false) String hasAnimation) throws AuthorizationFailedException {
        controleerAuthorisatie(authorizationHeader);

        return travelsService.get(country, name, converteerNaarBoolean(hasPool), converteerNaarBoolean(hasAnimation));
    }

    @PostMapping("/campings")
    @ResponseBody
    @CrossOrigin
    public ResponseEntity postCampings(@RequestBody Camping camping) {
        travelsService.post(camping);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        return new ResponseEntity("{\"message\":\"post succesful\"}", headers, HttpStatus.CREATED);
    }

    @DeleteMapping("/campings")
    @ResponseBody
    @CrossOrigin
    public ResponseEntity deleteCampings(@RequestParam(value = "index") int index) {
        travelsService.delete(index);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        return new ResponseEntity("{\"message\":\"Camping succesfully removed\"}", headers, HttpStatus.OK);
    }

    private void controleerAuthorisatie(String authorizationHeader) throws AuthorizationFailedException {
        if (authorizationHeader == null || !authorizationHeader.startsWith("Basic ")) {
            throw new AuthorizationFailedException("The way is shut!");
        }
        String decodedHeader = decodeAuthorizationHeader(authorizationHeader.replace("Basic ", ""));
        if (!decodedHeader.equals(String.format("%s:%s", httpUsername, httpPassword))) {
            throw new AuthorizationFailedException("The way is shut!");
        }
    }

    @ExceptionHandler(AuthorizationFailedException.class)
    @ResponseBody
    @CrossOrigin
    public String geenToegang() {
        return "{\"error\": \"The way is shut!\"}";

    }

    private String decodeAuthorizationHeader(String header) {
        byte[] x = DatatypeConverter.parseBase64Binary(header);
        return new String(x);
    }

    private Boolean converteerNaarBoolean(String booleanString) {
        if (booleanString == null) {
            return null;
        } else if (booleanString.equals("yes")) {
            return true;
        } else if (booleanString.equals("no")) {
            return false;
        } else {
            return null;
        }
    }

}