package nl.praegus.travels.backend.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Campings {
    @JsonProperty(value = "campings")
    private List<Camping> campings;

    public void add(Camping camping) {
        campings.add(camping);
    }

    public void remove(int index) {
        this.campings = campings.stream().filter(c -> c.getIndex() != index).collect(Collectors.toList());
    }
}
