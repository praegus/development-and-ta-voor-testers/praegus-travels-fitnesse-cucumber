function postCamping(country, pool, anim, city, price, stars, name) {
    $.ajax({
        type: 'POST',
        url: 'http://localhost:8005/campings/',
        data: JSON.stringify({"pricePerNight": price, "hasAnimation": anim, "stars": stars, "name": name, "hasPool": pool, "country": country, "city": city}),
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            console.log(data);
            $("#inputs").remove();
            $("[prg_name='add']").replaceWith("<p class=\"okMessage\">Camping is succesvol toegevoegd!</p><a href=\"/newV2.html\" title=\"Nog een camping toevoegen\">Voeg nog een camping toe</a>");

        },
        error: function () {
            console.log("Error occured while posting new camping");
            $("[prg_name='add']").replaceWith("<p class=\"errorMessage\">Foutmelding met nuttige informatie ;-)</p>");

        }
    });
}

$(document).ready(function () {
    $("[prg_name='add']").click(function () {
        var country, pool, anim, city, price, stars, name;
        country = $("[prg_name='camping.country']").val();
        pool = $("[prg_name='camping.pool']").val();
        anim = $("[prg_name='camping.anim']").val();
        city = $("[prg_name='camping.city']").val();
        price = $("[prg_name='camping.price']").val();
        stars = $("[prg_name='camping.stars']").val();
        name = $("[prg_name='camping.name']").val();
        postCamping(country, pool, anim, city, price, stars, name);
    });
});