function postCamping(country, pool, anim, city, price, stars, name) {
    $.ajax({
        type: 'POST',
        url: 'http://localhost:8005/campings/',
        data: JSON.stringify({"pricePerNight": price, "hasAnimation": anim, "stars": stars, "name": name, "hasPool": pool, "country": country, "city": city}),
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            console.log(data);
            $("[prg_name='add']").replaceWith("<p class=\"okMessage\">Camping is succesvol toegevoegd!</p><a href=\"/new.html\" title=\"Nog een camping toevoegen\">Voeg nog een camping toe</a>");
        },
        error: function () {
            console.log("Error occured while posting new camping");
            $("[prg_name='add']").replaceWith("<p class=\"errorMessage\">Foutmelding met nuttige informatie ;-)</p>");

        }
    });
}

$(document).ready(function () {
    $("[prg_name='add']").click(function () {
        var country, pool, anim, city, price, stars, name;
        country = $("[prg_name='country']").val();
        pool = $("[prg_name='pool']").val();
        anim = $("[prg_name='anim']").val();
        city = $("[prg_name='city']").val();
        price = $("[prg_name='price']").val();
        stars = $("[prg_name='stars']").val();
        name = $("[prg_name='name']").val();
        postCamping(country, pool, anim, city, price, stars, name);
    });
});