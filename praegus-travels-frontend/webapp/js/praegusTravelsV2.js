function getCampings(country, pool, anim) {
    var name;
    var city;
    var pricePerNight;
    var stars;
    var htmlContent;
    var qString;
    var hasPool = false;
    var photo;
    var i = 0;
    var dummyContent;
    var password = 'Test123';
    var username = 'Tester';

    dummyContent = "<div class=\"col\">";
    dummyContent += "<h2><a href=\"#\">Adverteren?</a></h2>";
    dummyContent += "<p><a href=\"#\" style=\"overflow:none;\"><img width=\"100%\" height=\"125\" src=\"camping-img/dummy.jpg\" class=\"block\" alt=\"\" /></a></p>";
    dummyContent += "<p class=\"smaller\"><strong>Hier had uw camping kunnen staan!</strong><br />Vestibulum at dolor vel risus scelerisque lobortis vitaehendrerit dui. Culi sociis natoque penatibus et magnis.<br />&nbsp;<br /></p>";
    dummyContent += "</div>";
    qString = country;

    qString = 'country=' + country;
    if (pool.length > 0) {
        hasPool = 'yes';
        qString += '&' + pool + '=' + hasPool;
    }

    if (anim.length > 0) {
        hasAnim = 'yes';
        qString += '&' + anim + '=' + hasAnim;
    }

    $.ajax({
        url: 'http://localhost:8005/campings?' + qString,
        dataType: 'JSON',
        type: 'GET',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Basic " + btoa(username + ":" + password));
        },
        success: function (data) {
            console.log(data);
            $(".cols3-content").html("");
            data.campings.forEach(function (entry) {
                name = entry.name;
                city = entry.city;
                pricePerNight = entry.pricePerNight;
                stars = entry.stars;
                var imgNum;
                if (hasPool === 'yes') {
                //01-12
                    imgNum = Math.floor(Math.random() * (12 - 1 + 1)) + 1;
                    photo = "camping-img/zwb/" + imgNum + ".jpg";
                } else {
                    imgNum = Math.floor(Math.random() * (26 - 1 + 1)) + 1;
                    photo = "camping-img/" + imgNum + ".jpg";
                }

                htmlContent = "<div class=\"col\">";
                htmlContent += "<h2><a href=\"#\">" + city + " - " + stars + " sterren </a></h2>";
                htmlContent += "<p><a href=\"#\" style=\"overflow:none;\"><img width=\"100%\" height=\"125\" src=\"" + photo + "\" class=\"block\" alt=\"\" /></a></p>";
                htmlContent += "<p class=\"smaller\"><strong>" + name + "</strong> Vestibulum at dolor vel risus scelerisque lobortis vitaehendrerit dui. Culi sociis natoque penatibus et magnis.<br />&nbsp;<br /></p>";
                htmlContent += "<span style=\" background: url('design/ul.gif') 10px 15px no-repeat; padding-left:25px; padding-top:10px;\">Prijs per nacht: " + pricePerNight + "</span></div>";

                $(".cols3-content").append(htmlContent);
                i++;
                if (i % 3 === 0) {
                    i = 0;
                }
            });
            if (i === 1) {
                $(".cols3-content").append(dummyContent);
                $(".cols3-content").append(dummyContent);
            } else if (i === 2) {
                $(".cols3-content").append(dummyContent);
            }
        }
    });
}

$(document).ready(function () {
    $("[prg_name='zoek']").click(function () {
        var country, pool, anim;
        country = $("[prg_name='country']").val();
        pool = $("[prg_name='pool']").val();
        anim = $("[prg_name='anim']").val();
        getCampings(country, pool, anim);
    });
});